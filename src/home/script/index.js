/* Styles */
const reset = require('../style/reset.css');
const fonts = require('../style/_fonts.scss');
const variables = require('../style/_variables.scss');
const animations = require('../style/_animations.scss');
const index = require('../style/index.scss');
const navbar = require('../style/navbar.scss');
const hero = require('../style/hero.scss');
const about = require('../style/about.scss');
const projects = require('../style/projects.scss');
const contact = require('../style/contact.scss');

/* Scripts */
const terminalTyper = require('./terminalTyper.js');
const intersectionObserver = require('./intersectionObserver.js');
const IOFallback = require('./IOFallback.js');

/* Media */
const typing = require('../../images/typing.mp4');