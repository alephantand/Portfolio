const animate = document.querySelector('.typing');
const animateContainer = document.querySelector('.typing-container');
const phrases = ['Matthew' ,'your next developer'];
const timeBetweenLetters = 150;
const timeBetweenBackspaces = 100;
const timeBeforeNextWord = 1000;
const timeBeforeBackspace = 1250;
let phrase = null;
let flag = false;
let i = 0;

/* Idle remains too long after initial erase. */
phrase = phrases[1];
let timeout = setTimeout(hold, timeBeforeNextWord);
let interval;

function init() {
  i = 0;
  animate.classList.add('idle');
  clearInterval(interval);
  clearTimeout(timeout);
  toggle();
}

function toggle() {
  if (flag) {
    flag = false;
    phrase = phrases[0];
    animateContainer.style.marginTop = '0';
    animate.style.fontSize = '60px'
  } else {
    flag = true;
    phrase = phrases[1];
    animateContainer.style.marginTop = '-8px';
    animate.style.fontSize = '40px';
  }
  interval = setInterval(type, timeBetweenLetters);
}

function type() {
  if (animate.textContent.length < phrase.length) {
    animate.textContent += phrase.charAt(i);
    i++;
  } else {
    animate.classList.remove('idle');
    clearInterval(interval);
    timeout = setTimeout(hold, timeBeforeBackspace);
  }
}

function hold(timeout) {
  animate.classList.remove('idle');
  clearTimeout(timeout);
  interval = setInterval(backspace, timeBetweenBackspaces);
}

function backspace() {
  animate.classList.add('idle');
  if (animate.textContent.length >= 1) {
    animate.textContent = animate.textContent.slice(0, -1);
  } else {
    animate.classList.remove('idle');
    clearInterval(interval);
    interval = setInterval(init, timeBeforeNextWord);
  }
}