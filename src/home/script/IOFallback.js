function dummyFunction(){};

if (new IntersectionObserver(dummyFunction, dummyFunction) == null) {
  const underlines = document.querySelectorAll('.underline');
  document.addEventListener('scroll', checkScrollY);

  underlines.forEach(element => {
    element.classList.add('shrink');
  });

  function checkScrollY() {
    underlines.forEach(element => {
      if (window.scrollY > element.getBoundingClientRect().top + 150) {
        element.classList.replace('shrink', 'expand');
      }
    })
  }
}