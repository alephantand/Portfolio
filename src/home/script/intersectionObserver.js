const separators = document.querySelectorAll('.separator');
const numSteps = 50.0;
const options = {
  root: null,
  rootMargin: '-200px',
  threshold: buildThresholdList()
}
let width = "ratiopx";

function buildThresholdList() {
  let thresholds = [];

  for (let i=1.0; i<=numSteps; i++) {
    var ratio = i/numSteps;
    thresholds.push(ratio);
  }

  thresholds.push(0);
  return thresholds;
}

let observerWidth = new IntersectionObserver(handleWidth, options);
if (observerWidth != null) {
  separators.forEach(element => {
    observerWidth.observe(element);
  });
}

function handleWidth(entries, observerWidth) {
  entries.forEach(entry => {
    entry.target.style.width = width.replace("ratio", entry.intersectionRatio  * 50);
  });
}